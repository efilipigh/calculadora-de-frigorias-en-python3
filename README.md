# Calculadora de frigorias en python3

Calculadora para obtener el valor de frigorias de un equipo de refrigeracion, respecto de un lugar de medidas X * Y , y altura Z.

Reportando el area cubierta en metros cuadrados, los metros cubicos del lugar y finalmente las frigorias minimas necesarias para la refrigeracion de dicho espacio.