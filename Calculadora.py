import os, string
banner= """
 e88~-_            888                  888                 888                           
d888   \   /~~~8e  888  e88~~\ 888  888 888   /~~~8e   e88~\888  e88~-_  888-~\   /~~~8e  
8888           88b 888 d888    888  888 888       88b d888  888 d888   i 888          88b 
8888      e88~-888 888 8888    888  888 888  e88~-888 8888  888 8888   | 888     e88~-888 
Y888   / C888  888 888 Y888    888  888 888 C888  888 Y888  888 Y888   ' 888    C888  888 
 "88_-~   "88_-888 888  "88__/ "88_-888 888  "88_-888  "88_/888  "88_-~  888     "88_-888 
                                                                                          
      888                                                                                 
 e88~\888  e88~~8e                                                                        
d888  888 d888  88b                                                                       
8888  888 8888__888                                                                       
Y888  888 Y888    ,                                                                       
 "88_/888  "88___/                                                                        
                                                                                          
888~~         ,e,       /                 ,e,                                             
888___ 888-~\  "  e88~88e  e88~-_  888-~\  "    /~~~8e   d88~\                            
888    888    888 888 888 d888   i 888    888       88b C888                              
888    888    888 "88_88" 8888   | 888    888  e88~-888  Y88b                             
888    888    888  /      Y888   ' 888    888 C888  888   888D                            
888    888    888 Cb       "88_-~  888    888  "88_-888 \_88P                             
                   Y8""8D                                                                 
"""
print(banner)
print("\n")
print("\n")
'''lado1 = 0
lado2 = 0
altura = 0'''

print("Dame la medida de uno de los lados de lugar")
print("\n")
lado1 = float(input())
print("\n")
print("Dame la medida de otro de los lados de lugar")
print("\n")
lado2 = float(input())
area = lado1 * lado2
print("\n")
print("El lugar tiene un area total cubierta de " + str(area))
print("\n")
print("Dame la altura del lugar")
print("\n")
altura = float(input())
print("\n")
mcubicos = round((area * altura),2)
print("\n")
print("En base al area del lugar y su altura, el lugar tiene " + str(mcubicos) + " metros cubicos a ser refrigerados")
print("\n")
frigorias = round(mcubicos * 50)
print("Se necesitan " + str(frigorias) + " como minimo, para refrigerar dicho lugar")
print("\n")
os.system("pause")